//Trabajador hereda de persona
public class Trabajador extends Persona{

	private double sueldo;

	
	public double getSueldo() 
	{
		return sueldo;
	}

	public void setSueldo(double sueldo) 
	{
		this.sueldo = sueldo;
	}

	public Trabajador()	//Un constructor vacio inicializa los atributos con valores por defecto
	{	
	}

	public Trabajador(String nombre, String apellidos,
			double sueldo)
	{//Este super llama al constructor de la clase base
	//que tiene dos parametros del tipo String 
	//En este caso sera:Persona(nombre,apellidos)
		super(nombre,apellidos);
		this.sueldo=sueldo;
		
	}
	
}
