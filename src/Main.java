import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Trabajador trabajador = new Trabajador();
		trabajador.setNombre("Pepe");
		trabajador.setApellidos("Soto");
		trabajador.setSueldo(1000.50);
		
		
		Trabajador trabajador2 = new Trabajador();
		trabajador2.setNombre("Pepe");
		trabajador2.setApellidos("Soto");
		trabajador2.setSueldo(1000.50);
		
		
		if(trabajador.equals(trabajador2))
		{
			System.out.println("Los Trabajadores son iguales");
		}
		else
		{
			System.out.println("Los Trabajadores son diferentes");
		}
		
		
		
		Trabajador trabajador1 = new Trabajador("Pepe","Soto",1000.50);
		
		//Polimorfismo de objetos o boxing/unboxing
		
		Persona persona=new Trabajador();
		
		//convert int to integer
		int i=0;
		Integer ri = Integer.valueOf(i);
		
		
		ArrayList Lista=new ArrayList();
		Lista.add("Pepe");
		Lista.add(trabajador);
		Object objetoTrabajador = Lista.get(1);
		((Trabajador)objetoTrabajador).setSueldo(1000.50);
		
		
		
		//Unboxing de una sola linea
		((Trabajador)persona).setSueldo(1000.50);
		if (persona instanceof Trabajador){
			Trabajador t=(Trabajador)persona;
			t.setSueldo(1000.50);
		}
		
		if (persona.getClass().equals(Trabajador.class)){
			Trabajador t=(Trabajador)persona;
			t.setSueldo(1000.50);
		}
		
		ArrayList<Trabajador> listaTrabajadores =
					new ArrayList<Trabajador>();
		listaTrabajadores.add(trabajador);
		//listaTrabajadores.add("Test");No se pueden agregar string en una
		//coleccion del tipo trabajador. Solo se pueden agregar objetos del
		//tipo trabajador o derivados.
		
		ArrayList<Persona> listaPersonas =
				new ArrayList<Persona>();
		Persona p1=new Persona();
		Trabajador t11 =new Trabajador();
		listaPersonas.add(p1);
		listaPersonas.add(t11);
		
		InstrumentoMusical im=new Guitarra();
		im.afinar();
		
		//Una variable puede tener multiples formas
		//el unboxing se hace en runtime
		//unboxing de 2 lineas
		Circulo circulo=new Circulo();
		circulo.calcularArea();
		circulo.dibujar();
		
		//otra opcion: t.setSueldo(1000.50);
		
		
	}

}
