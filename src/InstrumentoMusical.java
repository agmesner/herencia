
public interface InstrumentoMusical {//En la interface no puedo crear metodos no abstractos
    void tocar();
    void afinar();
    String tipoInstrumento();

}
